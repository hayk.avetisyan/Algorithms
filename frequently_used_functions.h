#include <iostream>

void swap(int &rInt1, int &rInt2)
{
    int temp = rInt1;
    rInt1 = rInt2;
    rInt2 = temp;
}

template<typename T>
void readArray(T array[], int arraySize)
{
    for (int index = 0; index < arraySize; ++index) {
        std::cin >> array[index];
    }
}

template<typename T>
void printArray(T array[], int arraySize)
{
    std::cout << std::endl;
    for (int index = 0; index < arraySize; ++index) {
        std::cout << array[index] << ' ';
    }
    std::cout << std::endl;
}



