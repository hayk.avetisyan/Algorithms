//////// Linear search ////////////////////////////////////
#include "../../frequently_used_functions.h"

/* Function to search int in an sorted array using binary search*/
void linearSearch(int arr[], const int& arrSize, const int& wanted)
{
    for (int index = 0; index < arrSize; ++index) {
        if (wanted == arr[index]) {
            std::cout << "Index of " << wanted << " is: " << index << "\n";
            return;
        } 
    }

    std::cout << wanted << " doesn't exist at the array\n";
}

int main()
{
    int arrSize = -999;
    std::cout << "Enter the size of the sorted array:\n";
    std::cin >> arrSize;
    int arr[arrSize];

    readArray(arr, arrSize);
    std::cout << "\nArray is:";
    printArray(arr, arrSize);

    std::cout << "\nEnter the number to be wanted in array:\n";
    int wanted = -999;
    std::cin >> wanted;
    linearSearch(arr, arrSize, wanted);

    return 0;
}

