//////// Insertion Sort ////////////////////////////////////
#include "../../frequently_used_functions.h"

/* Function to search int in an sorted array using binary search*/
void binarySearch(int arr[], const int& arrSize, const int& wanted)
{
    int loIndex = 0;
    int hiIndex = arrSize - 1;
    while (wanted != arr[hiIndex] && loIndex != hiIndex) {
       int middle = loIndex + (hiIndex - loIndex) / 2;
       if (wanted > arr[middle]) {
           loIndex = middle + 1;
       } else {
           hiIndex = middle;
       }
    }

    if (wanted == arr[hiIndex]) {
        std::cout << "Index of " << wanted << " is: " << hiIndex << "\n";
        return;
    } 

    std::cout << wanted << " doesn't exist at the array\n";
}

int main()
{
    int arrSize = -999;
    std::cout << "Enter the size of the sorted array:\n";
    std::cin >> arrSize;
    int arr[arrSize];

    readArray(arr, arrSize);
    std::cout << "\nArray is:";
    printArray(arr, arrSize);

    std::cout << "\nEnter the number to be wanted in array:\n";
    int wanted = -999;
    std::cin >> wanted;
    binarySearch(arr, arrSize, wanted);

    return 0;
}

