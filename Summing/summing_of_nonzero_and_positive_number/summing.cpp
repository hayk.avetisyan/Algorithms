//////// Merge Sort ////////////////////////////////////
#include "../../frequently_used_functions.h"

/* Function to calculate the sum of array's elements*/
double sum(double arr[], int arrSize) 
{   
    // if array's element is zero, we reach to the end of inputted array 
    size_t index = 0;
    double sum = 0;
    while (0 != arr[index]) {
        sum += arr[index];
        ++index;
    }

    return sum;
}

int main()
{
    const int SIZE_OF_ARRAY = 100;
    double arr[SIZE_OF_ARRAY] = {0};

    // inputted elements must be none zero for correct result
    readArray(arr, SIZE_OF_ARRAY); 
    std::cout << "\nInputted array:";
    printArray(arr, SIZE_OF_ARRAY);

    std::cout << "\nsum of array elements: " << sum(arr, SIZE_OF_ARRAY) << "\n";

    return 0;
}

