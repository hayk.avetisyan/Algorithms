#ifndef TREENODE_H
#define TREENODE_H

#include "../../std_lib_facilities_haykav.h" /// all include stuff
#include <iterator>


template <typename TREETYPE>
class TreeNode {
    template <typename TYPEOFTREE> friend class Tree;

private:
    int childIndex;
    int deepth;
    TREETYPE key;
    vector<TreeNode<TREETYPE>*> childVec;
    TreeNode<TREETYPE> * parentNode;
public:

    class IteratorOfTree {
    private:
        TreeNode<TREETYPE> * initialTreeNode;
        TreeNode<TREETYPE> * curr;
    public:

        IteratorOfTree()
            :curr{nullptr}, initialTreeNode{nullptr}
        { }

        IteratorOfTree(TreeNode<TREETYPE> * ptreeArg)
            :curr{ptreeArg}, initialTreeNode{ptreeArg}
        { }

        static IteratorOfTree END_iterator;
        bool RightBrotherExist() const
        {
            if ( nullptr == curr->parentNode )
                return false;

            if ( curr != curr->parentNode->childVec.back() ) 
                return true;
            return false;
        }

        void GoToRightBrother()
        {
            if ( RightBrotherExist() ) 
                curr = curr->parentNode->childVec[ curr->childIndex + 1 ];
        }

        bool operator==(const IteratorOfTree &iterArg) const
        {
            return ( curr == iterArg.curr && initialTreeNode == iterArg.initialTreeNode );
        }

        bool operator!=(const IteratorOfTree &iterArg) const
        {
            return !(*this == iterArg);
        }

        IteratorOfTree & operator++() 
        {
            if ( !curr->isExternal() ) {
                curr = curr->childVec[0];
                return *this;
            }

            if ( RightBrotherExist() ) {
                GoToRightBrother();
                return *this;
            }

            TreeNode<TREETYPE> * currFirst = curr;
            curr = curr->parentNode;
            while ( nullptr != curr && !RightBrotherExist() ) {
                curr = curr->parentNode;
            }
            
            if (nullptr == curr) {
                initialTreeNode = nullptr;
                return *this;
            }

            GoToRightBrother();
            return *this;
        }

        TREETYPE & operator*()
        {
            return curr->key;
        }
    };

public:
    IteratorOfTree begin()
    {
        if (isRoot()) { 
            return IteratorOfTree(this);
        }

        error("begin() function must be applied on root");
    }

    IteratorOfTree end()
    {
        IteratorOfTree zeroIter;
        return zeroIter;
    }

    explicit TreeNode(const TREETYPE &keyArg)
        : key( keyArg ), deepth(0), parentNode(nullptr)
    { }

    ~TreeNode() // not completed
    {
        parentNode = nullptr;
    }

    TreeNode<TREETYPE>(const TreeNode<TREETYPE> & treeArg) 
    {
        key = treeArg.key;
        childVec = treeArg.childVec;
        parentNode = nullptr;
    }

    TreeNode<TREETYPE> * parent() const { return parentNode; }
    vector<TreeNode<TREETYPE>*> children() const { return childVec; }
    bool isRoot() const {return (nullptr == parentNode); }
    bool isExternal() const 
    {
        if (childVec.empty()) return true;
        return false;
    }

    void insertAsChild(TreeNode<TREETYPE> * ptreeArg) 
    {
        childVec.push_back(ptreeArg);
        ptreeArg->parentNode = this;
        ptreeArg->childIndex = childVec.size() - 1;
    }

    void insertUnderKey(const TREETYPE &keyArg, TreeNode<TREETYPE> * ptreeArg) 
    {
        if (key == keyArg) {
            insertAsChild(ptreeArg);
        }

        if ( !isExternal() ) {
            for (int i = 0; i < childVec.size(); ++i) {
                childVec[i]->insertUnderKey(keyArg, ptreeArg);
            }
        }
    }

    void printTree()
    {
        printSpaces(2 * deepth);
        cout << key << "\n";
        if ( !isExternal() ) {
            for (int i = 0; i < childVec.size(); ++i) {
                childVec[i]->deepth = deepth + 1;
                childVec[i]->printTree();
            }
        }
    }
};

#endif
