#ifndef TREE_H
#define TREE_H

#include "TreeNode.h"

template <typename TREETYPE> 
class Tree {
private:
    TreeNode<TREETYPE> * root;

public:
    class DFS_tree_iterator {
    private:
        TreeNode<TREETYPE> * curr;
        TreeNode<TREETYPE> * rootInIter;
    public:
        DFS_tree_iterator()
            :curr{NULL}, rootInIter{NULL}
        { }

        DFS_tree_iterator(const Tree &treeArg)
            :curr{treeArg.root}, rootInIter{treeArg.root}
        { }

        bool RightBrotherExist() const
        {
            if ( NULL == curr->parentNode )
                return false;

            if ( curr != curr->parentNode->childVec.back() ) 
                return true;
            return false;
        }

        void GoToRightBrother()
        {
            if ( RightBrotherExist() ) 
                curr = curr->parentNode->childVec[ curr->childIndex + 1 ];
        }

        bool operator==(const DFS_tree_iterator &iterArg) const
        {
            return (curr == iterArg.curr && rootInIter == iterArg.rootInIter);
        }

        bool operator!=(const DFS_tree_iterator &iterArg) const
        {
            return !(*this == iterArg);
        }

        DFS_tree_iterator & operator++() 
        {
            if ( !curr->isExternal() ) {
                curr = curr->childVec[0];
                return *this;
            }

            if ( RightBrotherExist() ) {
                GoToRightBrother();
                return *this;
            }

            TreeNode<TREETYPE> * currFirst = curr;
            curr = curr->parentNode;
            while ( NULL != curr && !RightBrotherExist() ) {
                curr = curr->parentNode;
            }
            
            if (NULL == curr) {
                rootInIter = NULL;
                return *this;
            }

            GoToRightBrother();
            return *this;
        }

        TREETYPE & operator*()
        {
            return curr->key;
        }

        TreeNode<TREETYPE> * getTreeNode()
        {
            return curr;
        }

    };

public:
    Tree() // default constructor without param
        : root{NULL}
    { }

    Tree(TreeNode<TREETYPE> * ptreeArg) // default constructor
    {
        if (NULL == ptreeArg->parentNode)
            root = ptreeArg;
    }

    explicit Tree(const TREETYPE & keyArg) // default constructor
    {

            root = new TreeNode<TREETYPE>(keyArg);
    }

    ~Tree()
    {
        delete [] root;
    }

    DFS_tree_iterator begin()
    {
        return DFS_tree_iterator(*this);
    }

    DFS_tree_iterator end()
    {
        DFS_tree_iterator zeroDFS_iterator;
        return zeroDFS_iterator;
    }

    void insertUnderRoot(TreeNode<TREETYPE> * ptreeArg)
    {
        root->childVec.push_back(ptreeArg);
        ptreeArg->parentNode = this;
        ptreeArg->childIndex = root->childVec.size() - 1;
    }

    void insertUnderKeysTreeNode(const TREETYPE &keyArg, TreeNode<TREETYPE> * ptreeArg)
    {
        for (DFS_tree_iterator it = begin(); it != end(); ++it) {
            if (keyArg == *it) {
                TreeNode<TREETYPE> * foundTreeNode = it.getTreeNode();
                foundTreeNode->childVec.push_back(ptreeArg);
                ptreeArg->parentNode = foundTreeNode;
                return;
            }
        }

        error("There are no TreeNode containing such key value");
    }

    void printTree()
    {
        root->printTree();
    }


};


#endif
