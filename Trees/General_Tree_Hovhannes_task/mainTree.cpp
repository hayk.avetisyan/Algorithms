//////// SOLUTION of  ////////////////////////////////////
#include "Tree.h"
#include <iterator>

/////////////////////// PROTOTYPES OF FUNCTIONS //////////////////////////////////////
// prototype for function template printList
template < typename T > void printList( const list< T > &listRef );
//////////////////////////////////////////////////////////////////////////////////////

int main()
{
    try {
        TreeNode<string> treeOfSTL("STL");
        treeOfSTL.insertAsChild(new TreeNode<string>("Containers"));
        treeOfSTL.insertAsChild(new TreeNode<string>("Iterators"));
        treeOfSTL.insertAsChild(new TreeNode<string>("Algorithms"));
        treeOfSTL.insertAsChild(new TreeNode<string>("Functors"));
        treeOfSTL.insertAsChild(new TreeNode<string>("queue"));

        treeOfSTL.insertUnderKey("Containers", new TreeNode<string>("Sequence containers"));
        treeOfSTL.insertUnderKey("Containers", new TreeNode<string>("Associative containers"));
        treeOfSTL.insertUnderKey("Containers", new TreeNode<string>("Container adaptors"));
        treeOfSTL.insertUnderKey("Sequence containers", new TreeNode<string>("vector"));
        treeOfSTL.insertUnderKey("Sequence containers", new TreeNode<string>("deque"));
        treeOfSTL.insertUnderKey("Sequence containers", new TreeNode<string>("list"));
        treeOfSTL.insertUnderKey("Associative containers", new TreeNode<string>("set"));
        treeOfSTL.insertUnderKey("Associative containers", new TreeNode<string>("multiset"));
        treeOfSTL.insertUnderKey("Associative containers", new TreeNode<string>("map"));
        treeOfSTL.printTree();
        cout << endl << endl;

        cout << "Printing with iterators:\n";
        for (TreeNode<string>::IteratorOfTree it = treeOfSTL.begin(); it != treeOfSTL.end(); ++it) {
            cout << " " << *it << "\n";
        }

        Tree<string> myRealTree(&treeOfSTL);

    } catch (exception& e) {
        cerr << "exception: " << e.what() << endl;
        return 1;
    } catch (...) {
        cerr << "exception\n";
        return 2;
    }
    
    return 0; /// program ended successfully
}/// end function main

/////////////////////// DEFINITIONS OF FUNCTIONS ////////////////////////
// printList function template definition; uses
// ostream_iterator and copy algorithm to output list elements
template < typename T > void printList( const list< T > &listRef )
{
    if ( listRef.empty() ) { // list is empty
        cout << "List is empty";
    } else {
        ostream_iterator< T > output( cout, " " );
        copy( listRef.cbegin(), listRef.cend(), output );
    } // end else
} // end function printList
