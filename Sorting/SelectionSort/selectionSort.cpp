//////// Selection Sort ////////////////////////////////////
#include "../../frequently_used_functions.h"


void selectionSort(int array[], int arraySize)
{
    int indexOfLast = arraySize - 1;
    for (int counter = 0; counter < indexOfLast; ++counter) {
        int indexOfMin = counter;

        for (int index = counter + 1; index <= indexOfLast; ++index) {
           if (array[index] < array[indexOfMin]) {
              indexOfMin = index;
           }
        }

        swap(array[counter], array[indexOfMin]);
    }
}

int main()
{
    int arraySize = -999;
    std::cout << "Enter the size of the array:\n";
    std::cin >> arraySize;
    int array[arraySize];

    readArray(array, arraySize);
    std::cout << "\nArray before sorting:";
    printArray(array, arraySize);

    selectionSort(array, arraySize);
    std::cout << "\nArray after \"Selection\" sorting:";
    printArray(array, arraySize);

    return 0;
}

