//////// Counting Sort ////////////////////////////////////
#include "../../frequently_used_functions.h"

/* Finding min and max of an array */
void findMinMax(const int arr[], const int arrSize, int& min, int& max)
{
    min = arr[0];
    max = arr[0];

    for (int i = 0; i < arrSize; ++i) {
        if (arr[i] < min) min = arr[i];
        if (arr[i] > max) max = arr[i];
    }
}

/* Function to sort an array using merge sort*/
void countingSort(int arr[], const int arrSize)
{
    int min = -999;
    int max = -999;
    findMinMax(arr, arrSize, min, max);

    const int countArrSize = max + 1;
    int countArr[countArrSize] = {0};

    // countArr contains the count of it's indexes in arr[] array
    for (int i = 0; i < arrSize; ++i) {
        ++countArr[arr[i]];
    }

    // adding the prev element to current element in countArr
    for (int j = 1; j < countArrSize; ++j) {
        countArr[j] = countArr[j] + countArr[j - 1];
    }

    int sortArr[arrSize] = {0};
    for (int i = arrSize - 1; i >= 0; --i) {
        sortArr[countArr[arr[i]] - 1] = arr[i]; 
        --countArr[arr[i]];
    }

    for (int i = 0; i < arrSize; ++i) {
        arr[i] = sortArr[i];
    }
}

int main()
{
    int arrSize = -999;
    std::cout << "Enter the size of the arr:\n";
    std::cin >> arrSize;
    int arr[arrSize];

    readArray(arr, arrSize);
    std::cout << "\nArray before sorting:";
    printArray(arr, arrSize);

    countingSort(arr, arrSize);
    std::cout << "\nArray after \"Counting\" sorting:";
    printArray(arr, arrSize);

    return 0;
}

