//////// Bubble Sort ////////////////////////////////////
#include "../../frequently_used_functions.h"

/* get next gap */
int getNextGap(int gap)
{
    gap = gap * 10 / 13;
    if (gap < 1) return 1;
    return gap;
}
     
/* Function to sort an array using comb sort*/
void combSort(int arr[], int arrSize)
{
    int gap = arrSize;
    bool swapped = true;

    while (1 != gap || true == swapped) {
        gap = getNextGap(gap);
        swapped = false;

        int upperLimit = arrSize - gap;
        for (int i = 0; i < upperLimit; ++i) {
            if (arr[i] > arr[i + gap]) {
                swap(arr[i], arr[i + gap]);
                swapped = true;
            }
        }
    }
}

int main()
{
    int arrSize = -999;
    std::cout << "Enter the size of the array:\n";
    std::cin >> arrSize;
    int array[arrSize];

    readArray(array, arrSize);
    std::cout << "\nArray before sorting:";
    printArray(array, arrSize);

    combSort(array, arrSize);
    std::cout << "\nArray after \"Comb\" sorting:";
    printArray(array, arrSize);

    return 0;
}

