//////// Counting Sort ////////////////////////////////////
#include "../../frequently_used_functions.h"
#include <cmath>
#include <vector>
#include <algorithm>

/* Function to sort an array using merge sort*/
void bucketSort(float arr[], const int arrSize)
{
    std::vector<float> vec[arrSize];
    for (int i = 0; i < arrSize; ++i) {
        int vecIndex = 10 * arr[i];
        vec[vecIndex].push_back(arr[i]);
    }

    for (int j = 0; j < 10; ++j) {
        std::sort(vec[j].begin(), vec[j].end());
    }

    int index = 0;
    for (int i = 0; i < arrSize; ++i) {
        for (int j = 0; j < vec[i].size(); ++j) {
            arr[index++] = vec[i][j];
        }
    }
}

int main()
{
    int arrSize = -999;
    std::cout << "Enter the size of the arr:\n";
    std::cin >> arrSize;
    float arr[arrSize];

    readArray(arr, arrSize);
    std::cout << "\nArray before sorting:";
    printArray(arr, arrSize);

    bucketSort(arr, arrSize);
    std::cout << "\nArray after \"Radix\" sorting:";
    printArray(arr, arrSize);

    return 0;
}

