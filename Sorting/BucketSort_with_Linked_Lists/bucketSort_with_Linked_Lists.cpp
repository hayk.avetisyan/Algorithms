//////// Counting Sort ////////////////////////////////////
#include "../../frequently_used_functions.h"
#include <cmath>
#include <list>
#include <algorithm>

/* Function to sort an array using merge sort*/
void bucketSort(float arr[], const int arrSize)
{
    std::list<float> listMy[arrSize];
    for (int i = 0; i < arrSize; ++i) {
        int listMyIndex = 10 * arr[i];
        listMy[listMyIndex].push_back(arr[i]);
    }

    for (int j = 0; j < 10; ++j) {
        listMy[j].sort();
    }

    int index = 0;
    for (int i = 0; i < arrSize; ++i) {
        for (std::list<float>::iterator it = listMy[i].begin(); it != listMy[i].end(); ++it) {
            arr[index++] = *it;
        }
    }
}

int main()
{
    int arrSize = -999;
    std::cout << "Enter the size of the arr:\n";
    std::cin >> arrSize;
    float arr[arrSize];

    readArray(arr, arrSize);
    std::cout << "\nArray before sorting:";
    printArray(arr, arrSize);

    bucketSort(arr, arrSize);
    std::cout << "\nArray after \"Bucket\" sorting with linked lists:";
    printArray(arr, arrSize);

    return 0;
}

