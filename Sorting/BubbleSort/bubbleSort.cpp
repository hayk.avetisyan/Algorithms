//////// Bubble Sort ////////////////////////////////////
#include "../../frequently_used_functions.h"

/* Function to sort an array using bubble sort*/
void bubbleSort(int array[], int arraySize)
{
    int indexOfLast = arraySize - 1;
    for (int counter = 1; counter <= indexOfLast; ++counter) {
        int upperLimit = indexOfLast - counter;

        /// the last: array[upperLimit + 1] element already is in place
        for (int index = 0; index <= upperLimit; ++index) {
            if (array[index] > array[index + 1]) {
                swap(array[index], array[index + 1]);
            }
        }
    }
}

int main()
{
    int arraySize = -999;
    std::cout << "Enter the size of the array:\n";
    std::cin >> arraySize;
    int array[arraySize];

    readArray(array, arraySize);
    std::cout << "\nArray before sorting:";
    printArray(array, arraySize);

    bubbleSort(array, arraySize);
    std::cout << "\nArray after \"Selection\" sorting:";
    printArray(array, arraySize);

    return 0;
}

