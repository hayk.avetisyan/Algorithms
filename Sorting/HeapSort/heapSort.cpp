//////// Merge Sort ////////////////////////////////////
#include "../../frequently_used_functions.h"

/* Function to sort an array using merge sort*/
void heapify(int arr[], const int& heapSize, const int& index)
{
    int indexOfMax = index;
    int indexOfLeft = 2 * index + 1;
    int indexOfRight = 2 * index + 2;

    if (indexOfLeft < heapSize && arr[indexOfLeft] > arr[indexOfMax]) {
        indexOfMax = indexOfLeft;
    }

    if (indexOfRight < heapSize && arr[indexOfRight] > arr[indexOfMax]) {
        indexOfMax = indexOfRight;
    }
    
    if (index != indexOfMax) {
        swap(arr[indexOfMax], arr[index]);
        heapify(arr, heapSize, indexOfMax);
    }
}

void heapSort(int arr[], int arrSize)
{
    for (int index = (arrSize / 2) + 1; index >= 0; --index) {
        heapify(arr, arrSize, index);
    }

    for (int index = arrSize - 1; index >= 0; --index) {
        swap(arr[0], arr[index]);
        heapify(arr, index, 0);
    }
}

int main()
{
    int arrSize = -999;
    std::cout << "Enter the size of the arr:\n";
    std::cin >> arrSize;
    int arr[arrSize];

    readArray(arr, arrSize);
    std::cout << "\nArray before sorting:";
    printArray(arr, arrSize);

    heapSort(arr, arrSize);
    std::cout << "\nArray after \"Heap\" sorting:";
    printArray(arr, arrSize);

    return 0;
}

