//////// Insertion Sort ////////////////////////////////////
#include "../../frequently_used_functions.h"

/* Function to sort an array using insertion sort*/
void shellSort(int arr[], int arrSize)
{
    for (int gap = arrSize / 2; gap > 0; --gap) {
        for (int i = 0; i < arrSize; ++i) {
            for (int j = i + gap; j < arrSize; j += gap) {
                if (arr[i] > arr[j]) {
                    swap(arr[i], arr[j]);
                    for (int k = i - gap; (arr[k] > arr[i] && k >= 0); k -= gap) {
                        swap(arr[k], arr[i]);
                    }
                }
            }
        }
    }
}

int main()
{
    int arraySize = -999;
    std::cout << "Enter the size of the array:\n";
    std::cin >> arraySize;
    int array[arraySize];

    readArray(array, arraySize);
    std::cout << "\nArray before sorting:";
    printArray(array, arraySize);

    shellSort(array, arraySize);
    std::cout << "\nArray after \"Shell\" sorting:";
    printArray(array, arraySize);

    return 0;
}

