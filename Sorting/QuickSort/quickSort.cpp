//////// Quick Sort ////////////////////////////////////
#include "../../frequently_used_functions.h"

/* Function to sort an array using merge sort*/
int partition(int arr[], int loIndex, int hiIndex)
{
    int pivot = arr[hiIndex];
    int i = loIndex - 1;
    for (int j = loIndex; j < hiIndex; ++j) {
        if (arr[j] < pivot) {
            ++i;
            swap(arr[j], arr[i]);
        }
    }

    swap(arr[i + 1], arr[hiIndex]);
    return i + 1;
}

void quickSort(int arr[], int loIndex, int hiIndex)
{
    if (loIndex < hiIndex) {
        int indexOfPivot = partition(arr,loIndex,hiIndex);
        quickSort(arr, loIndex, indexOfPivot - 1);
        quickSort(arr, indexOfPivot + 1, hiIndex);
    }
}

int main()
{
    int arrSize = -999;
    std::cout << "Enter the size of the arr:\n";
    std::cin >> arrSize;
    int arr[arrSize];

    readArray(arr, arrSize);
    std::cout << "\nArray before sorting:";
    printArray(arr, arrSize);

    quickSort(arr, 0, arrSize - 1);
    std::cout << "\nArray after \"Quick\" sorting:";
    printArray(arr, arrSize);

    return 0;
}

