//////// Merge Sort ////////////////////////////////////
#include "../../frequently_used_functions.h"

/* Function to sort an array using merge sort*/
void merge(int array[], int l, int m, int r)
{
    const int LEFT_ARRAY_SIZE = m - l + 1;
    const int RIGHT_ARRAY_SIZE = r - m;
    int L[LEFT_ARRAY_SIZE], R[RIGHT_ARRAY_SIZE];

    for (int indexLeft = 0; indexLeft < LEFT_ARRAY_SIZE; ++indexLeft) {
        L[indexLeft] = array[indexLeft + l];
    }

    for (int indexRight = 0; indexRight < RIGHT_ARRAY_SIZE; ++indexRight) {
        R[indexRight] = array[indexRight + m + 1];
    }

    int indexLeft = 0;
    int indexRight = 0;
    int indexOrig = l;
    while (indexLeft < LEFT_ARRAY_SIZE && indexRight < RIGHT_ARRAY_SIZE) {
        if (L[indexLeft] <= R[indexRight]) {
            array[indexOrig] = L[indexLeft];
            ++indexLeft;
        } else {
            array[indexOrig] = R[indexRight];
            ++indexRight;
        }

        ++indexOrig;
    }

    while (indexLeft < LEFT_ARRAY_SIZE) {
        array[indexOrig] = L[indexLeft];
        ++indexOrig;
        ++indexLeft;
    }

    while (indexRight < RIGHT_ARRAY_SIZE) {
        array[indexOrig] = R[indexRight];
        ++indexOrig;
        ++indexRight;
    }
}

void mergeSort(int array[], int l, int r)
{
    if (r > l) {
        int m = (l + r) / 2;
        mergeSort(array, l, m);
        mergeSort(array, m + 1, r);
        merge(array, l, m, r);
    }
}

int main()
{
    int arraySize = -999;
    std::cout << "Enter the size of the array:\n";
    std::cin >> arraySize;
    int array[arraySize];

    readArray(array, arraySize);
    std::cout << "\nArray before sorting:";
    printArray(array, arraySize);

    mergeSort(array, 0, arraySize - 1);
    std::cout << "\nArray after \"Merge\" sorting:";
    printArray(array, arraySize);

    return 0;
}

