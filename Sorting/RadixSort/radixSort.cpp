//////// Counting Sort ////////////////////////////////////
#include "../../frequently_used_functions.h"
#include <cmath>

/* Finding min and max of an array */
void findMinMax(const int arr[], const int arrSize, int& min, int& max)
{
    min = arr[0];
    max = arr[0];

    for (int i = 0; i < arrSize; ++i) {
        if (arr[i] < min) min = arr[i];
        if (arr[i] > max) max = arr[i];
    }
}

/* Function to sort an array using merge sort*/
void countingSortByDigit(int arr[], const int arrSize, const int digitNumber)
{
    int decOrder = pow(10, digitNumber - 1);
    int min = -999;
    int max = -999;
    findMinMax(arr, arrSize, min, max);

    const int countArrSize = max + 1;
    int countArr[countArrSize] = {0};

    // countArr contains the count of it's indexes in arr[] array
    for (int i = 0; i < arrSize; ++i) {
        int j = (arr[i] / decOrder) % 10;
        ++countArr[j];
    }

    // adding the prev element to current element in countArr
    for (int j = 1; j < countArrSize; ++j) {
        countArr[j] = countArr[j] + countArr[j - 1];
    }

    int sortArr[arrSize] = {0};
    for (int i = arrSize - 1; i >= 0; --i) {
        int j = (arr[i] / decOrder) % 10;
        sortArr[countArr[j] - 1] = arr[i]; 
        --countArr[j];
    }

    for (int i = 0; i < arrSize; ++i) {
        arr[i] = sortArr[i];
    }
}

void radixSort(int arr[], const int arrSize, const int numberOfDigits)
{
    for (int countDig = 1; countDig <= numberOfDigits; ++countDig) {
        countingSortByDigit(arr, arrSize, countDig);
    }
}

int main()
{
    int arrSize = -999;
    std::cout << "Enter the size of the arr:\n";
    std::cin >> arrSize;
    int arr[arrSize];

    readArray(arr, arrSize);
    std::cout << "\nArray before sorting:";
    printArray(arr, arrSize);

    radixSort(arr, arrSize, 3);
    std::cout << "\nArray after \"Radix\" sorting:";
    printArray(arr, arrSize);

    return 0;
}

