//////// Insertion Sort ////////////////////////////////////
#include "../../frequently_used_functions.h"

/* Function to sort an array using insertion sort*/
void insertionSort(int array[], int arraySize)
{
    for (int counter = 1; counter < arraySize; ++counter) {
        int key = array[counter];
        int index = counter - 1;

        /* Move elements of array[0..counter-1], that are greater than 
        key, to one position ahead of their current position */
        while (index >= 0 && key < array[index]) {
            array[index + 1] = array[index];
            --index;
        }

        array[index + 1] = key;
    }
}

int main()
{
    int arraySize = -999;
    std::cout << "Enter the size of the array:\n";
    std::cin >> arraySize;
    int array[arraySize];

    readArray(array, arraySize);
    std::cout << "\nArray before sorting:";
    printArray(array, arraySize);

    insertionSort(array, arraySize);
    std::cout << "\nArray after \"Selection\" sorting:";
    printArray(array, arraySize);

    return 0;
}

